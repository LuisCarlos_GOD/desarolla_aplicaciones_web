**Desarrollo de Aplicaciones Web con conexiones a base de datos**
**Alumno:**
*Carlos Fernando Angulo López*
**Semestre:**
*5AVP*


-Practica #1 - 02/09/2022 - Practica_ejemplo
commit: 0209b103eadb37ebe1036898d56f05ce58c41957
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/practica_ejemplo.html

-Práctica #2 - 09/09/2022 - Práctica JavaScript
commit: 8bc72d33c3a7d11271fa83848507dbdc7b1ef7c8
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

-Práctica #3 - 15/09/2022 - Práctica Web con base de datos - Parte 1
commit: 8bc72d33c3a7d11271fa83848507dbdc7b1ef7c8
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebApp.rar

-Práctica #4 - 19/09/2022 - Práctica Web con Bases de Datos - Vista de consulta de datos
commit: 8bc72d33c3a7d11271fa83848507dbdc7b1ef7c8
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/parcial1/Pr%C3%A1cticaWebDatos/consultarDatos.php

-Práctica #5 - 22/09/2022 - Práctica Web con Bases de Datos - Vista de registro de datos
commit: ecc2cd5793528fe077528e5d6e10999ff1400c0a
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/parcial1/Pr%C3%A1cticaWebDatos/registroDatos.html

-Práctica #6 - 26/09/2022 - Práctica Web conexión
commit: 8bc72d33c3a7d11271fa83848507dbdc7b1ef7c8
Archivo: https://gitlab.com/LuisCarlos_GOD/desarolla_aplicaciones_web/-/blob/main/parcial1/conexion.php
